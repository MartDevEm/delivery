"""Delivery_ElBuenSabor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from Delivery_ElBuenSabor.gestion_articulo import views as views_gestion_articulo



urlpatterns = [
    path('admin/', admin.site.urls),
    path('rubro_articulo/', views_gestion_articulo.Rubro_Articulo_ViewSet.as_view()),
    path('rubro_articulo/<int:pk>/',views_gestion_articulo.Rubro_Articulo_SetDetail.as_view()),
    path('articulo/', views_gestion_articulo.Articulo_ViewSet.as_view(),name='articulo-list'),
    path('articulo/<int:pk>/', views_gestion_articulo.Articulo_SetDetail.as_view(), name='articulo-detail'),
    path('articulo_manufacturado/', views_gestion_articulo.Articulo_Manufacturado_ViewSet.as_view(), name='articulo_manufacturado-list'),
    path('articulo_manufacturado/<int:pk>', views_gestion_articulo.Articulo_Manufacturado_SetDetail.as_view(), name='articulo_manufacturado-detail'),
    path('articulo_manufacturado_detalle/', views_gestion_articulo.Articulo_Manufacturado_Detalle_ViewSet.as_view(),name='articulo_manufacturado_detalle-list'),
    path('articulo_manufacturado_detalle/<int:pk>', views_gestion_articulo.Articulo_Manufacturado_Detalle_SetDetail.as_view(),name='articulo_manufacturado_datalle-detail'),
]
