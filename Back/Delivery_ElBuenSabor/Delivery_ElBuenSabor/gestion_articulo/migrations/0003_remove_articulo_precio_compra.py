# Generated by Django 2.2.1 on 2020-02-04 14:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gestion_articulo', '0002_auto_20200203_2032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='articulo',
            name='precio_compra',
        ),
    ]
