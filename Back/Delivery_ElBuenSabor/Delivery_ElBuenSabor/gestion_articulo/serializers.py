from .models import Rubro_Articulo
from .models import Articulo
from .models import Articulo_Manufacturado
from .models import Articulo_Manufacturado_Detalle
from rest_framework import serializers


class Rubro_Articulo_Serializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Rubro_Articulo
        fields = ("id", "denominacion")


class Articulo_Serializer(serializers.HyperlinkedModelSerializer):

    rubro_articulo = serializers.PrimaryKeyRelatedField(queryset=Rubro_Articulo.objects.all())

    class Meta:

        model = Articulo
        fields = ["id", "denominacion","precio_compra","precio_venta","stock_actual","unidad_medida", "es_insumo", "rubro_articulo"]



class Articulo_Manufacturado_Serializer(serializers.HyperlinkedModelSerializer):

    class Meta:

        model = Articulo_Manufacturado
        fields = ["id","tiempo_estimado_cocina","denominacion","precio_venta"]


class Articulo_Manufacturado_Detalle_Serializer(serializers.HyperlinkedModelSerializer):

    articulo_manufacturado = serializers.PrimaryKeyRelatedField(queryset=Articulo_Manufacturado.objects.all())
    articulo = serializers.PrimaryKeyRelatedField(queryset=Articulo.objects.all())

    class Meta:

        model = Articulo_Manufacturado_Detalle
        fields = ["id","cantidad","articulo_manufacturado","articulo"]


