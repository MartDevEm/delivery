from django.shortcuts import render
from .models import Rubro_Articulo, Articulo, Articulo_Manufacturado, Articulo_Manufacturado_Detalle
from rest_framework import viewsets
from .serializers import Rubro_Articulo_Serializer, Articulo_Serializer, Articulo_Manufacturado_Serializer ,Articulo_Manufacturado_Detalle_Serializer
from rest_framework import generics

class Rubro_Articulo_ViewSet(generics.ListCreateAPIView):

    queryset = Rubro_Articulo.objects.all()
    serializer_class = Rubro_Articulo_Serializer


class Rubro_Articulo_SetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Rubro_Articulo.objects.all()
    serializer_class = Rubro_Articulo_Serializer


class Articulo_ViewSet(generics.ListCreateAPIView):

    queryset = Articulo.objects.all()
    serializer_class = Articulo_Serializer


class Articulo_SetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Articulo.objects.all()
    serializer_class = Articulo_Serializer


class Articulo_Manufacturado_ViewSet(generics.ListCreateAPIView):

    queryset = Articulo_Manufacturado.objects.all()
    serializer_class = Articulo_Manufacturado_Serializer


class Articulo_Manufacturado_SetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Articulo_Manufacturado.objects.all()
    serializer_class = Articulo_Manufacturado_Serializer


class Articulo_Manufacturado_Detalle_ViewSet(generics.ListCreateAPIView):

    queryset = Articulo_Manufacturado_Detalle.objects.all()
    serializer_class = Articulo_Manufacturado_Detalle_Serializer


class Articulo_Manufacturado_Detalle_SetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Articulo_Manufacturado_Detalle.objects.all()
    serializer_class = Articulo_Manufacturado_Detalle_Serializer



