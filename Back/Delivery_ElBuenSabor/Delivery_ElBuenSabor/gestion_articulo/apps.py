from django.apps import AppConfig


class GestionArticuloConfig(AppConfig):
    name = 'gestion_articulo'
