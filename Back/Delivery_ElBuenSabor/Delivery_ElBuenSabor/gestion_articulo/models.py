from django.db import models

class Rubro_Articulo(models.Model):
    
    id = models.AutoField(primary_key= True)
    denominacion = models.TextField()


class Articulo(models.Model):

    id = models.AutoField(primary_key=True)
    denominacion = models.TextField()
    precio_compra = models.FloatField(default=0.00)
    precio_venta = models.FloatField(default=0.00)
    stock_actual = models.FloatField(default=0)
    unidad_medida = models.CharField(max_length=40)
    es_insumo = models.BooleanField()
    rubro_articulo = models.ForeignKey(
            Rubro_Articulo, related_name = 'articulo', on_delete=models.CASCADE, null=True)


class Articulo_Manufacturado(models.Model):

    id = models.AutoField(primary_key=True)
    tiempo_estimado_cocina = models.FloatField(default=0)
    denominacion = models.CharField(max_length=150)
    precio_venta = models.FloatField(default=0)


class Articulo_Manufacturado_Detalle(models.Model):

    id = models.AutoField(primary_key=True)
    cantidad = models.FloatField(default=True)
    articulo_manufacturado = models.ForeignKey(Articulo_Manufacturado, related_name='articulo_manufacturado', on_delete=models.CASCADE, null=True)
    articulo = models.ForeignKey(Articulo, related_name='articulo_manufacturado', on_delete=models.CASCADE, null= True)




